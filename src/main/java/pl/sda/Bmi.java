package pl.sda;

public class Bmi {
    public static boolean isOk (float weight, int height) {
        float heightcm = height;
        heightcm /= 100;
        float bmi = weight / (heightcm * heightcm);
        return bmi >= 18.5 && bmi <= 24.9;
    }
}
