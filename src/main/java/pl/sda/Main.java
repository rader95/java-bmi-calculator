package pl.sda;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        float weight;
        int height;
        Scanner scanner = new Scanner(System.in);
        weight = scanner.nextFloat();
        height = scanner.nextInt();
        boolean result = Bmi.isOk(weight, height);
        if (result) System.out.println("BMI optymalne");
        else System.out.println("BMI nieoptymalne");
    }
}
